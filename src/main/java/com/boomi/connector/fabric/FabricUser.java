// Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.fabric;

import java.security.PrivateKey;
import java.util.HashSet;
import java.util.Set;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.User;

public class FabricUser implements User {
	private final String name;
	private final String mspId;
	private final PrivateKey privateKey;
	private final String cert;

	/**
	 * Creating a new user
     */
	public FabricUser(String name, String mspId, PrivateKey privateKey, String cert) {
		this.name = name;
		this.mspId = mspId;
		this.privateKey = privateKey;
		this.cert = cert;
	}

	@Override
	public String getAccount() {
		return "";
	}

	@Override
	public String getAffiliation() {
		return "";
	}

	@Override
	public Enrollment getEnrollment() {
        return new Enrollment() {
			@Override
			public String getCert() {
				return cert;
			}

			@Override
			public PrivateKey getKey() {
				return privateKey;
			}
        };
	}

	@Override
	public String getMspId() {
		return mspId;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Set<String> getRoles() {
		return new HashSet<>();
	}
}
