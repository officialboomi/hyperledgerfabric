// Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.fabric;

import java.io.InputStream;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hyperledger.fabric.sdk.ChaincodeResponse.Status;
import org.hyperledger.fabric.sdk.ProposalResponse;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.fabric.util.FabricPayloadUtil;
import com.boomi.connector.util.BaseUpdateOperation;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FabricOperation extends BaseUpdateOperation {
	private static final Logger LOG = Logger.getLogger("com.boomi.connector.fabric");
	private FabricOperationType operationType;

	/**
	 * Instantiates a new connector 
	 *
	 * @param conn - the conn
	 * @param type - whether the operation type is Invoke or Query
	 */
	protected FabricOperation(FabricConnection conn, FabricOperationType type) {
		super(conn);
		operationType = type;
	}
	
	/**
	 * This method is used to perform Invoke and Query operation for HLF connector
	 */
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		PropertyMap opProperties = getContext().getOperationProperties();
		String function = opProperties.getProperty(FabricConstants.CHAINCODE_FUNCTION);
		Fabcar fabcar = new Fabcar();
		for (ObjectData od : request) {
			ObjectMapper mapper =  new ObjectMapper().disable(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS);
			try(JsonParser jsonParser = mapper.getFactory().createParser(od.getData());) {
				while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
					// Read a fabcar instance using ObjectMapper
					fabcar = mapper.readValue(jsonParser, Fabcar.class);
					break;
				}
				if (fabcar.getChaincodeId() == null) {
					response.addErrorResult(od, OperationStatus.FAILURE, String.valueOf(404), "ChaincodeId property in the incoming JSON Object is null", null);
					continue;
				}
				if (fabcar.getArgs() == null) {
					response.addErrorResult(od, OperationStatus.FAILURE, String.valueOf(404), "Args property in the incoming JSON Object is null", null);
					continue;
				}
				String chaincodeId = fabcar.getChaincodeId();

				switch (operationType) {
				case INVOKE: {
					ProposalResponse proposalResponse = getConnection().invoke(chaincodeId, function, fabcar.getArgs());
					if (proposalResponse != null) {
						LOG.log(Level.INFO, "Invoke Function "+function+" failed ["+proposalResponse.getMessage()+"]");
						response.addErrorResult(od, OperationStatus.FAILURE, String.valueOf(proposalResponse.getStatus().getStatus()), proposalResponse.getMessage(), null);
					}
					else {	
						response.addEmptyResult(od, OperationStatus.SUCCESS, String.valueOf(200), OperationStatus.SUCCESS.name());
					}
					break;
				}
				case QUERY: {
					Collection<ProposalResponse> responses = getConnection().query(chaincodeId, function, fabcar.getArgs());
					boolean isResponseNotBlank = false;
					if(responses.iterator().hasNext()) {
						ProposalResponse r = responses.iterator().next();
						if (r.getStatus() == Status.SUCCESS) {
							try(InputStream payload = r.getISChaincodeActionResponsePayload();) {

								if(function.equalsIgnoreCase("queryAllCars"))
								{
									try(JsonParser parser = mapper.getFactory().createParser(payload);){
										while (parser.nextToken() != null) {
											if (parser.getCurrentToken() == JsonToken.START_ARRAY) {
												while (parser.getCurrentToken() != JsonToken.END_ARRAY) {
													parser.nextToken();
													if (parser.getCurrentToken() == JsonToken.START_OBJECT) {
														ResponseUtil.addPartialSuccess(response, od,OperationStatus.SUCCESS.name(), FabricPayloadUtil.toPayload(parser));
														isResponseNotBlank = true;
													}
												}
												break;
											}
										}
									}
								}
								else {
									try(JsonParser parser = mapper.getFactory().createParser(payload);){
										while (parser.nextToken() != null) {
											if (parser.getCurrentToken() == JsonToken.START_OBJECT) {
												while (parser.getCurrentToken() != JsonToken.END_OBJECT) {
													ResponseUtil.addPartialSuccess(response, od,OperationStatus.SUCCESS.name(), FabricPayloadUtil.toPayload(parser));
													isResponseNotBlank = true;
												}
												break;
											}
										}
									}
								}
								if(isResponseNotBlank) {								
									response.finishPartialResult(od);
								}
								else{
									ResponseUtil.addEmptySuccess(response, od, OperationStatus.SUCCESS.name());
								}
							} 
						}
						else {
							LOG.log(Level.INFO, "Query Function "+function+" failed ["+r.getMessage()+"]");
							response.addErrorResult(od, OperationStatus.APPLICATION_ERROR, String.valueOf(r.getStatus().getStatus()), r.getMessage(), null);
						}
					}
					break;
				}
				}
			}catch(Exception e) {
				LOG.log(Level.SEVERE, "Failed ", e);
				ResponseUtil.addExceptionFailure(response, od, e);
			}
		}
	}
	/**
	 * getConnection
	 */
	@Override
	public FabricConnection getConnection() {
		return (FabricConnection) super.getConnection();
	}
}
