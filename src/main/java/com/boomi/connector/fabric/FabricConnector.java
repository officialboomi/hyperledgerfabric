// Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.fabric;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;

public class FabricConnector extends BaseConnector {
	@Override
	public Browser createBrowser(BrowseContext context) {
		return new FabricBrowser(createConnection(context));
	}

	@Override
	protected Operation createExecuteOperation(OperationContext context) {
		String customOperationType = context.getCustomOperationType();
		if (customOperationType.equals(FabricOperationType.INVOKE.name())) {
			return new FabricOperation(createConnection(context), FabricOperationType.INVOKE);
		} else if (customOperationType.equals(FabricOperationType.QUERY.name())) {
			return new FabricOperation(createConnection(context), FabricOperationType.QUERY);
		} else {
			throw new UnsupportedOperationException();
		}
	}

    private FabricConnection createConnection(BrowseContext context) {
        return new FabricConnection(context);
    }
}
