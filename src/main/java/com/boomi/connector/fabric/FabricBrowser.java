// Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.fabric;

import java.io.IOException;
import java.util.Collection;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.fabric.util.FabricUtil;
import com.boomi.connector.util.BaseBrowser;
import com.boomi.util.StringUtil;

public class FabricBrowser extends BaseBrowser {

	protected FabricBrowser(FabricConnection conn) {
		super(conn);
	}

	/**
	 * Creates an ObjectDefinition based on the input provided during profile
	 * import.
	 * @return objectDefinitions
	 */
	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId, Collection<ObjectDefinitionRole> roles) {
	    ObjectDefinitions defs = new ObjectDefinitions();
		try {
	        ObjectDefinition def = new ObjectDefinition();
	        def.setInputType(ContentType.JSON);
	        def.setOutputType(ContentType.JSON);
	        def.setElementName(StringUtil.EMPTY_STRING);
	        if (objectTypeId.equals("FabricArgs")) {
	        	def.setJsonSchema(FabricUtil.readJsonSchema("fabric_args_schema.json"));
	        }
		    defs.getDefinitions().add(def);
		} catch (IOException e) {
			throw new ConnectorException(e.getMessage());
		}
	    
		return defs;
	}
	
	/**
	 * Will return all the 'Fabric Arguments' as Object types
	 * 
	 * @return objectTypes
	 */
	@Override
	public ObjectTypes getObjectTypes() {
	    ObjectTypes objectTypes = new ObjectTypes();
        ObjectType deployOT = new ObjectType();
        deployOT.setId("FabricArgs");
        deployOT.setLabel("Fabric Arguments");
        objectTypes.getTypes().add(deployOT);
	    
		return objectTypes;
	}
}
