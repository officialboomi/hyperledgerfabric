// Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.fabric;

public enum FabricOperationType {
	INVOKE,
	QUERY
}
