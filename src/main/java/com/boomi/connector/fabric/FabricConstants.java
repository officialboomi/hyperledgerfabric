// Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.fabric;

public class FabricConstants {
	
	private FabricConstants(){
	}
	
	/** The Constant CHAINCODE_FUNCTION. */
	public static final String CHAINCODE_FUNCTION = "function";

}
