// Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.fabric;

import java.io.IOException;
import java.security.PrivateKey;
import java.util.Collection;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.ChaincodeID;
import org.hyperledger.fabric.sdk.ChaincodeResponse.Status;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.QueryByChaincodeRequest;
import org.hyperledger.fabric.sdk.TransactionProposalRequest;
import org.hyperledger.fabric.sdk.User;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.hyperledger.fabric.sdk.security.CryptoSuite;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.PrivateKeyStore;
import com.boomi.connector.api.PublicKeyStore;
import com.boomi.connector.fabric.util.FabricUtil;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.StringUtil;

public class FabricConnection extends BaseConnection {
    
    private static final String GRPC_PREFIX = "grpc://";
    private static final String GRPCS_PREFIX = "grpcs://";
    private static final String PEER_NAME = "peer";
    private static final String ORDERER_NAME = "orderer";
    private static final String IS_SECURE = "isSecure";
    private static final String USERNAME = "userName";
    private static final String USERMSPID = "userMSPId";
    private static final String USERCERT = "userCert";
    private static final String HOST = "host";
    private static final String PEERPORT = "peerPort";
    private static final String ORDERERPORT = "ordererPort";
    private static final String CHANNELNAME = "channnelName";
    private static final String PEERPUBLICCERT = "peerPublicCert";
    private static final String ORDERERPUBLICCERT = "ordererPublicCert";
    private static final String PEEROVERRIDE = "peerHostnameOverride";
    private static final String ORDEREROVERRIDE = "ordererHostnameOverride";
    private static final String PEMBYTES = "pemBytes";
    private static final String HOSTNAMEOVERRIDE = "hostnameOverride";
    
    private final HFClient client;
    private Channel channel = null;
    
    /**
	 * @param context
	 */
	public FabricConnection(BrowseContext context) {
		super(context);
		client = HFClient.createNewInstance();
		
		String userName = context.getConnectionProperties().getProperty(USERNAME);
        if (StringUtil.isEmpty(userName)) {
            throw new IllegalStateException("A user name must be provided");
        }
		
		String userMSPId = context.getConnectionProperties().getProperty(USERMSPID);
        if (StringUtil.isEmpty(userMSPId)) {
            throw new IllegalStateException("A user MSP Id must be provided");
        }
		PrivateKeyStore privateKeyStore = context.getConnectionProperties().getPrivateKeyStoreProperty(USERCERT);
		if (privateKeyStore == null) {
            throw new IllegalStateException("A user certificate must be provided");
		}
        
		String host = context.getConnectionProperties().getProperty(HOST);
        if (StringUtil.isEmpty(host)) {
            throw new IllegalStateException("A host must be provided");
        }
        
		Long peerPort = context.getConnectionProperties().getLongProperty(PEERPORT);
		if (peerPort == null || peerPort < 1 ) {
			throw new IllegalStateException("A peer port must be provided");
		}
		
		Long ordererPort = context.getConnectionProperties().getLongProperty(ORDERERPORT);
		if (ordererPort == null || ordererPort < 1 ) {
			throw new IllegalStateException("A orderer port must be provided");
		}
		
		String channelName = context.getConnectionProperties().getProperty(CHANNELNAME);
        if (StringUtil.isEmpty(host)) {
            throw new IllegalStateException("A channel name must be provided");
        }

		try {
			CryptoSuite cs = CryptoSuite.Factory.getCryptoSuite();
			client.setCryptoSuite(cs);

			PrivateKey privateKey = FabricUtil.getPrivateKeyFromKeyStore(privateKeyStore.getKeyStore(), "fabric", privateKeyStore.getPassword());
			String cert = FabricUtil.getCertificateAsPEMFromKeyStore(privateKeyStore.getKeyStore(), "fabric");
			
			User user = new FabricUser(userName, userMSPId, privateKey, cert);
			
			client.setUserContext(user);

			channel = client.newChannel(channelName);
	        
	        Boolean isSecure = context.getConnectionProperties().getBooleanProperty(IS_SECURE, false);
	        if (isSecure) {
	        	PublicKeyStore peerPublicCert = context.getConnectionProperties().getPublicKeyStoreProperty(PEERPUBLICCERT);
	        	if (peerPublicCert == null) {
	        		throw new IllegalStateException("A public peer certificate must be provided");
	        	}
	        	PublicKeyStore ordererPublicCert = context.getConnectionProperties().getPublicKeyStoreProperty(ORDERERPUBLICCERT);
	        	if (ordererPublicCert == null) {
	        		throw new IllegalStateException("A public orderer certificate must be provided");
	        	}
	        	Properties peerProps = new Properties();
	        	byte[] peerCertBytes = FabricUtil.getCertificateAsPEMFromKeyStore(peerPublicCert.getKeyStore()).getBytes();
				peerProps.put(PEMBYTES, peerCertBytes);
	        	String peerHostnameOverride = context.getConnectionProperties().getProperty(PEEROVERRIDE);
	        	if (peerHostnameOverride != null) {
	    			peerProps.put(HOSTNAMEOVERRIDE, peerHostnameOverride);
	        	}
	        	Properties ordererProps = new Properties();
	        	byte[] orderCertBytes = FabricUtil.getCertificateAsPEMFromKeyStore(ordererPublicCert.getKeyStore()).getBytes();
				ordererProps.put(PEMBYTES, orderCertBytes);
	        	String ordererHostnameOverride = context.getConnectionProperties().getProperty(ORDEREROVERRIDE);
	        	if (ordererHostnameOverride != null) {
	    			ordererProps.put(HOSTNAMEOVERRIDE, ordererHostnameOverride);
	        	}
	        	channel.addPeer(client.newPeer(PEER_NAME, GRPCS_PREFIX+host+":"+peerPort, peerProps));
	        	channel.addOrderer(client.newOrderer(ORDERER_NAME, GRPCS_PREFIX+host+":"+ordererPort, ordererProps));
	        } else {
	        	channel.addPeer(client.newPeer(PEER_NAME, GRPC_PREFIX+host+":"+peerPort));
	        	channel.addOrderer(client.newOrderer(ORDERER_NAME, GRPC_PREFIX+host+":"+ordererPort));
	        }
			channel.initialize();
		} catch (Exception e) {
			throw new ConnectorException("Fail to connect to Fabric ["+host+"]", e.getMessage());
		}
	}
	
	/**
	 * This function queries the fabric ledger 
	 * @param chaincodeId - name of the chaincode that you passed while installing chaincode
	 * @param functionName - chaincode function name passed in the input (queryCar/queryAllCars)
	 * @param args - args passed in the input
	 */
	protected Collection<ProposalResponse> query(String chaincodeId, String functionName, String [] args) throws InvalidArgumentException, ProposalException {
        QueryByChaincodeRequest req = client.newQueryProposalRequest();
        ChaincodeID cid = ChaincodeID.newBuilder().setName(chaincodeId).build();
        req.setChaincodeID(cid);
        req.setFcn(functionName);
        req.setArgs(args);
		return channel.queryByChaincode(req);
	}
	
	/**
	 * This function updates/creates record in the fabric ledger 
	 * @param chaincodeId - name of the chaincode that you passed while installing chaincode
	 * @param functionName - chaincode function name passed in the input (createCar/changeCarOwner)
	 * @param args - args passed in the input
	 */
	protected ProposalResponse invoke(String chaincodeId, String functionName, String [] args) throws InvalidArgumentException, ProposalException, InterruptedException, ExecutionException, TimeoutException, IOException  {
        TransactionProposalRequest req = client.newTransactionProposalRequest();
        ChaincodeID cid = ChaincodeID.newBuilder().setName(chaincodeId).build();
        req.setChaincodeID(cid);
        req.setFcn(functionName);
        req.setArgs(args);
		Collection<ProposalResponse> resps = channel.sendTransactionProposal(req);
		for (ProposalResponse response : resps) {
			if (response.getStatus() != Status.SUCCESS) {
				return response;
			}
		}
		channel.sendTransaction(resps);
    	BlockEvent.TransactionEvent event = channel.sendTransaction(resps).get(120, TimeUnit.SECONDS);
    	if (!event.isValid()) {
    		throw new IOException("Error sending transaction to "+chaincodeId+" "+functionName+" transaction id "+event.getTransactionID());
    	}
        return null;
	}
}
