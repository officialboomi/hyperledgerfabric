// Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.fabric;
/**
 * @author Aditi Ardhapure
 *
 * ${tags}
 */

public class Fabcar {
	/**
	 * POJO to get chaincodeId and args[] 
	 */
	private String chaincodeId;
	private String[] args;
	
	public Fabcar() {
		super();
	}
	public String[] getArgs() {
		return args;
	}
	public void setArgs(String[] args) {
		this.args = args;
	}
	public String getChaincodeId() {
		return chaincodeId;
	}
	public void setChaincodeId(String chaincodeId) {
		this.chaincodeId = chaincodeId;
	}
	
}
