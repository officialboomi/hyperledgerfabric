// Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.fabric.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import javax.xml.bind.DatatypeConverter;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.json.JSONException;
import com.boomi.util.IOUtil;

public class FabricUtil {
    public static final String UTF8 = "UTF-8";
    
    private FabricUtil() {
    	
    }
    /**
     * Takes the inputstream to convert to a string
     * @return
     * @throws IOException
     */
    public static String toString(InputStream in, String charsetName) throws IOException {
    	ByteArrayOutputStream bout = new ByteArrayOutputStream();
    	byte[] buf = new byte[8192];
    	try{
    		for( int len; ( len = in.read(buf) ) != -1; ) {
    			bout.write(buf, 0, len);
    		}
    		return bout.toString(charsetName);
    	}finally {
    		IOUtil.closeQuietly(in);
    		IOUtil.closeQuietly(bout);
    	}
    }
    /**
     * Method that reads fabric_args_schema.json
     * @throws JSONException
     * @throws IOException
     */
	public static String readJsonSchema(String path) throws IOException {
		InputStream is = null;
	    try {
	    	is = FabricUtil.class.getResourceAsStream("/"+path);
            return toString(is, UTF8);
        } finally {
        	IOUtil.closeQuietly(is);
        }
	}
	/**
	 *  Generates the private key using alias and password
	 * @throws UnrecoverableKeyException
	 * @throws KeyStoreException
	 * @throws NoSuchAlgorithmException
	 */
	public static PrivateKey getPrivateKeyFromKeyStore(KeyStore keystore, String alias, String pwd) throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException {
		return (PrivateKey)keystore.getKey(alias, pwd.toCharArray());
	}
	
	/**
	 * Generates private key from the PEM file
	 * @throws IOException
	 */
	public static PrivateKey getPrivateKeyFromPEM(String pem) throws IOException {
        PEMParser parser = new PEMParser(new StringReader(pem));
        PrivateKeyInfo keyInfo = (PrivateKeyInfo)parser.readObject();
		JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
		parser.close();
		return converter.getPrivateKey(keyInfo);
    }

	/**
	 * Returns the certificate
	 * @throws KeyStoreException
	 * @throws CertificateEncodingException
	 */
	public static String getCertificateAsPEMFromKeyStore(KeyStore keystore) throws KeyStoreException, CertificateEncodingException {
		return getCertificateAsPEMFromKeyStore(keystore, keystore.aliases().nextElement());
	}

	/**
	 * Returns the certificate
	 * @throws KeyStoreException
	 * @throws CertificateEncodingException
	 * @throws IOException
	 */
	public static String getCertificateAsPEMFromKeyStore(KeyStore keystore, String alias) throws KeyStoreException, CertificateEncodingException {
		Certificate c = keystore.getCertificate(alias);
		StringWriter sw = new StringWriter();
		sw.write("-----BEGIN CERTIFICATE-----\n");
        sw.write(DatatypeConverter.printBase64Binary(c.getEncoded()).replaceAll("(.{64})", "$1\n"));
        sw.write("\n-----END CERTIFICATE-----\n");		
		return sw.toString();
	}
}
