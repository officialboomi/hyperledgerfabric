For forking and pull requests. Ensure you are logged in with your Bitbucket account. Fork repository via the sitemap: https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html
If fork is complete. Please submit a pull request. We'd love to review your code and consider it into the mainline

---

# Building this connector project #
To build this java - maven connector project, you will need to download and unpack the latest (or recent) version of [Maven](https://maven.apache.org/download.cgi) and put the mvn command on your path. 
Then, you will need to install a Java 1.7 preferably, as most of the connectors are designed to work with minimum JDK 1.7 (or higher) JDK (not JRE!), and make sure you can run java from the command line. 
Now you can run `mvn clean` and then `mvn install`. Maven will compile your project, and put the results in the target directory.

---

# Hyperledger Fabric connector #
Hyperledger connector allows you to transfer any amount of data securely from/to system connecting to Blockchain through AWS Instance. The Blockchain is a secure transaction ledger that is shared by all parties participating in an established, distributed network of computers. Through this connector, you can use a Dell Boomi Integration process to Invoke (create, update) and Query (Get) the data in JSON format. 
> _**Note:**_ This connector only works with Local Atom and does not work with Cloud Atom.

## Connector configuration ##
To configure the connector to communicate with Blockchain, please set up two components:

* Hyperledger connection
* Hyperledger operation

This design provides reusable components containing connection settings and operation settings. After building your connection and operation, set up your connector within a process. When the process is defined properly, Boomi Integration can map to and from virtually any system using this connector to communicate with Blockchain.

## Prerequisites ##

To use the connector and implement a connection to your MongoDB server from Boomi Integration, do the following:
* Host and port (Peer, Orderer) details of Blockchain.
* Username and password with necessary credentials.
* AWS Instance ready with below applications installed.
* Deploy Atom on your local machine or host it with Boomi Integration.
* Username and password with the necessary credentials and any one of the below authentication types
	* cURL
	* Docker and Docker Compose
	* Go Programming Language
	* Node.js Runtime and NPM
	* Python (for Ubuntu 16.04 users only).
> _**Note:**_ Currently this connector uses the repository with source code and sample data provided by the client which should be cloned in AWS instance for a successful process. The necessary certificates are generated using this cloned repository within the AWS instance.
* Create below necessary certificate files that can be imported into Boomi
	* User Certificate.
	* Public certificates for the orderer and peer.
	* Java Fabric SDK with associated Jar files in the Dell Boomi environment.


## Supported editions ##

The connector supports below editions
* Docker and Docker Compose
* For Windows 10: version 17.06.2-ce or greater is required.
* Older versions of Windows: version 17.06.2-ce or greater is required along with Docker Toolbox.
* Go Programming Language: Go - version 1.9.x
* Node.js - version 8.9.x or greater
* Python (for Ubuntu 16.04 users only): By default, Ubuntu 16.04 comes with Python 3.5.1 installed as the python3 binary. The Fabric Node.js SDK requires an iteration of Python 2.7 for npm install operations to complete successfully. Retrieve the 2.7 version with the following command:

	`sudo apt-get install python`

## Tracked properties ##
This connector has no tracked properties that you can set or reference in various shape parameters.
See the topic [Adding tracked fields to a connector operation](http://help.boomi.com/atomsphere/GUID-C84D1FEF-BD90-46CE-BFD2-33CE720572EE.html) to learn how to add a custom tracked field.

## Additional resources ##
* [https://hyperledger-fabric.readthedocs.io/en/release-1.1/prereqs.html](https://hyperledger-fabric.readthedocs.io/en/release-1.1/prereqs.html)
* [https://hyperledger-fabric.readthedocs.io/en/release-1.1/write_first_app.html](https://hyperledger-fabric.readthedocs.io/en/release-1.1/write_first_app.html)


# Hyperledger connection #
The Hyperledger connection represents a single account including login credentials. If you have multiple systems, you need a separate connection for each.

## Connection tab ##

### Name ###

### Description ###

### User Name ###
User name of the user.

### User MSP Id ###
MSP ID (Membership Service Provider) of the user is a component that abstracts away all cryptographic mechanisms and protocols behind issuing and validating certificates, and user authentication. An MSP may define their notion of identity and the rules by which those identities are governed (identity validation) and authenticated (signature generation and verification). This Can be located under the ‘User Name’ file. 

### User Certificate ###
You will need to create a certificate file that can be imported into Boomi
* In the "User" file the "signing identity" property identifies the private key file. It is this id +"-priv". 
* Extract the "certificate" value and save it to a text file. This is the public certificate
* Run `openssl pkcs12 -name fabric -export -out <cert name>.p12 -inkey <private key file> -in <public cert file>`
* Use the <certificate name>.p12 generated to create an X.509 Certificate component. This is used for the User Certificate property of the Connection

### Channel Name ###
Enter the channel name through which each transaction on the network is executed.

### Host ###
A domain name or IP address of Blockchain.

### Peer Port ###
Port number of Peer.

### Orderer Port ###
Port number of Orderer

### Use Secure Connection (Optional) ###
If checked, then the URL prefix is set to "grpcs://" instead of "grpc://".

### Peer Public Certificate ###
This certificate is used in SDK that connects to a particular peer. Below is the code to generate the certificate
`openssl x509 -outform der -in <folder location>-cert.pem -out <cert name>.der`

### Peer Hostname Override ###
The hostname override is used when validating the connection to the orderer.

### Orderer Public Certificate ###
This certificate is used in SDK that connects to the particular orderer. Below is the code to generate the certificate.
`openssl x509 -outform der -in <folder location>-cert.pem -out <cert name>.der`

### Orderer Hostname Override ###
The hostname override is used when validating the connection of the orderer.

> _**Note:**_ * If the certificates are provided in PEM form, they can be converted to DER form which can then be used to create the Public Certificate components. 
* Once the certificates are generated, you need to import all generated certificates in the Boomi platform. This can be done by adding a new certificate component.
* Since this connector is currently using the repository with source code and sample data provided by the client which should be cloned in AWS instance for a successful process. The necessary certificates are generated using this cloned repository within the AWS instance

# Hyperledger operation #
The Hyperledger operations define how to interact with Blockchain and represent a specific action (Query and Invoke) to be performed against a specific file.
Create a separate operation component for each action/object combination that your integration requires.
The Hyperledger operations support the following actions:
* Inbound: Query
* Outbound: Invoke
All functions expect a JSON Object with a chaincodeId property string and a JSON array of strings as input arguments. The format of response data returned from query function calls will be dictated by the smart contract.

## Options Tab ##
Select a connector action and then use the Import Wizard to select the object with which you want to integrate. When you configure an action (Query and Invoke), the following fields appear on the Options tab.

## Invoke ##
Invoke instantiates transactions in a channel. Invoke perform read/write operation on the Blockchain.
 "Fabric Arguments" is the only object type that Invoke supports 
There are two functions within invoke operation "createCar" and "changeCarOwner".

### Name ###

### Description ###

### Connector Action ###
Determines the type of operation the connector is performing related to Inbound or Outbound, specify the proper connector action. Depending on how you create the operation component, the action type is either configurable or non-configurable from the drop-down list. 

### Object ###
Defines the object with which you want to integrate, and which was selected in the Import Wizard. The Object for Query operation is "Fabric Arguments".

### Request Profile ###
Select or add a JSON profile component that represents the structure that is being sent by the connector.

### Tracking Direction ###
Displays the document tracking direction for write and Execute operations, either Input documents or Output documents. This read-only setting is determined by a configuration change, affecting which document appears in Process Reporting.

### Return Application Error Responses ###
This setting controls whether an application error prevents an operation from completing:
* If cleared, the process aborts and reports the error on the Process Reporting page.
* If selected, processing continues and passes the error response to the next component to be processed as the connection output.


### Invoke ChainCode Function ###
The chaincode is the 'smart contract' that runs on the peers and creates transactions. A smart contract defines the transaction logic that controls the object contained.
Below are two functions within Query operation
* createCar – to create a new record
e.g.: `{"chaincodeId": "fabcar","args": ["CAR20","Red","Chevy","Volt","Nick"]}`
* changeCarOwner – to update an existing record
e.g.: `{"chaincodeId": "fabcar","args": ["CAR2021","Nicholas"]}`
> _**Note:**_ This field needs to be filled manually based on the function to be performed within query operation. The below functions may vary based on the data and other functions available.


## Query ##
The query is an inbound action that looks up objects (File) in Blockchain and returns zero to many object records from a single Query request based on specific search criteria.
"Fabric Arguments" is the only object type that Query supports 
There are two functions within query operation "queryCar" and "queryAllCars".


### Name ###

### Description ###

### Connector Action ###
Determines the type of operation the connector is performing related to Inbound or Outbound, specify the proper connector action. Depending on how you create the operation component, the action type is either configurable or non-configurable from the drop-down list. 

### Object ###
Defines the object with which you want to integrate, and which was selected in the Import Wizard. The Object for Query operation is "Fabric Arguments".

### Request Profile ###
Select or add a JSON profile component that represents the structure that is being sent by the connector.

### Tracking Direction ###
This read-only setting shows you the tracking direction (either Input Documents or Output Documents) for the current operation. The default value for Query operation is Output Documents. This setting is determined by the operation configuration in the connector descriptor and affects which document appears in Process Reporting. See the @trackedDocument attribute in the [Connector descriptor files](https://help.boomi.com/bundle/connectors/page/c-atm-Connector_descriptor_files_3ad80d9a-217c-4fe4-b347-e885d156e0dc.html) topic for more information
### Return Application Error Responses ###
This setting controls whether an application error prevents an operation from completing:
* If cleared, the process aborts and reports the error on the Process Reporting page.
* If selected, processing continues and passes the error response to the next component to be processed as the connection output.

### Query ChainCode Function ###
The chaincode is the 'smart contract' that runs on the peers and creates transactions. A smart contract defines the transaction logic that controls the object contained.
Below are two functions within Query operation
* queryCar – for specific record
e.g.: `{"chaincodeId": "fabcar", "args": ["CAR9"]}`
* queryAllCars – to get all records together
e.g.:  `{"chaincodeId": "fabcar", "args": [""]}`

> _**Note:**_ This field needs to be filled manually based on the function to be performed within query operation. The below functions may vary based on the data and other functions available.

## Archiving tab ##
See the [Connector operation’s Archiving tab](https://help.boomi.com/bundle/connectors/page/r-atm-Connector_operations_Archiving_tab_061fbf70-1034-4bf3-b795-e952f9338dbe.html) for more information.

## Tracking tab ##
See the [Connector operation’s Tracking tab](https://help.boomi.com/bundle/connectors/page/r-atm-Connector_operations_Tracking_tab_8a03f547-738a-448c-bb0f-594bad806cfe.html) for more information.

## Caching tab ##
See the [Connector operation’s Caching tab](https://help.boomi.com/bundle/connectors/page/r-atm-Connector_operations_Caching_tab_f46b49d6-25bc-4337-ade1-9c67817b8d74.html) for more information.

# Importing Jar files in Boomi Platform #
By adding customized configuration files from a JAR file, you extend the functionality of the Hyperledger connector and your integration processes.
For local Atoms and Molecules, you can add customized Hyperledger configuration files as a configuration resource. To support this functionality, you Upload the "connector-fabric-0.0.x-car.zip" file and the "connector-descriptor.xml" file via the Boomi Setup/Developer panel.
> _**Note:**_ It is mandatory to upload all the necessary (Total 41 files) JAR Files.
 
## Procedure ##
* Place the JAR file in the local system folder.
* Log in to the Dell Boomi Platform.
* Go to Setup and Account Libraries at the left corner.
* Upload the custom JAR file to an account by using the [Manage Account Libraries page](https://help.boomi.com/bundle/connectors/page/r-atm-Account_Library_Management_edc37905-b4fe-4cae-8001-b62221adb872.html) (Setup > Account Libraries). To upload files, you must have the Build Read and Write Access privilege and the Developer privilege. 

> _**Note:**_ Uploaded or imported files are first passed through a virus scanner. The upload or import results in an error if a virus is detected, and the file is rejected. Please contact Dell Boomi Support if an error persists.
* Create a [Custom Library component](https://help.boomi.com/bundle/connectors/page/t-atm-Creating_a_Custom_Library_Component_8fce64fb-4b85-4977-9876-e0d616526228.html) in the process. Select "Connector" for Custom Library Type and select "Connector Name" for Connector Type and add the uploaded JAR file in the account library to the component. 
* Created "Custom Library" component must be referenced to the installed connector (Set the Custom Library type to "Connector" and the Connector type to be what you named the uploaded connector).

# Setting up AWS instance #
Setting up an AWS instance is a one-time process that is mandatory for a successful process.
For information on how to set AWS instance please [click here](https://docs.aws.amazon.com/efs/latest/ug/getting-started.html).
For information for installing Prerequisite applications within AWS instance, please [click here](https://hyperledger-fabric.readthedocs.io/en/release-1.1/prereqs.html).
> _**Note:**_ The AWS instance setup with all installed prerequisite applications is an onetime setup and can be used for multiple operations.

# Import certificates into Boomi Platform #
For the Hyperledger connector, three types of certificates should be imported.
* User Certificate.
* Peer Public Certificate.
* Orderer Public Certificate.

## Procedure ##
* Click the dropdown arrow at the side of the respective folder under which the certificate should be created and select "New Component".
* A create component window will be displayed and fill in the following fields.
* Type: Select the "Certificate" component form the dropdown.
* Component Name: User-defined.
* Folder: By default, the path displayed will be the selected folder. It can be updated if necessary.
* Certificate Type: Select X.509 from the dropdown. The type should be mandatorily X.509.
* Once created, use the import option at the right corner to upload the necessary certificates stored in your local system when they are created.
* Post successful import, the certificates are added under the respective folder.
> _**Note:**_ While importing the user certificate, the password must be provided which is set during certificate creation.
